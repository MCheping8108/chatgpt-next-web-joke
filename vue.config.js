module.exports = {
    // ...其他配置
  
    // 配置webpack
    configureWebpack: {
      output: {
        // 输出到 'dist' 目录
        path: __dirname + '/dist',
  
        // 为 Chrome 插件准备
        filename: 'js/content.js'
      }
    }
  }
  